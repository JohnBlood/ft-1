# FT-1

A fortune-telling program with a very advanced algorithm written in BASIC

[Windows executable](https://gitlab.com/JohnBlood/ft-1/-/blob/master/FT-1.exe)

[Linux executable](https://gitlab.com/JohnBlood/ft-1/-/blob/master/FT-1)

If you have a Mac, you can easily create your own executable by downloading [QB64](https://www.qb64.org/portal/) and downloading FT-1.bas. Open the .bas file in QB64 and hit F5.
